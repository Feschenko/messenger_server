const express = require('express');
const path = require('path');
const cookieSession = require('cookie-session');
const bcrypt = require('bcryptjs');
const dbConnection = require('./database');
const { body, validationResult } = require('express-validator');

const http = require('http');

const PORT = process.env.PORT || 3000;
const app = express();
const server = http.createServer(app);
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");

const jwt = require("jsonwebtoken");

const jwtKey = "my_secret_key";
const jwtExpirySeconds = 86400;

let corsOptions = {
  headers: '*',
  origin: [
    'http://25.88.24.170:3000', 
    'http://localhost:3000', 
  ],
  optionsSuccessStatus: 200
};

let multer  = require('multer');
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

app.use(cors(corsOptions));
app.use(express.urlencoded({extended:false}));

app.set('views', path.join(__dirname,'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(cookieParser());

app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2']
}));

app.post('/register', cors(corsOptions), 
[
  body('user_email', 'Invalid email address!').isEmail().custom((value) => {
    return dbConnection.execute('SELECT `email` FROM `users` WHERE `email`=?', [value])
    .then(([rows]) => {
      if(rows.length > 0){
        return Promise.reject('This E-mail already in use!');
      }
      return true;
    });
  }),
  body('user_name', 'Username is Empty!').trim().not().isEmpty(),
  body('user_pass', 'The password must be of minimum length 8 characters').trim().isLength({ min: 8 }),
], (req, res, next) => {
  const validation_result = validationResult(req);
  const {user_name, user_pass, user_email} = req.body;
  if(validation_result.isEmpty()) {
    bcrypt.hash(user_pass, 12).then((hash_pass) => {
      dbConnection.execute("INSERT INTO `users`(`username`,`email`,`password`) VALUES(?,?,?)", [user_name, user_email, hash_pass])
      .then(result => {
        dbConnection.execute("SELECT * FROM `users` WHERE `email`=?", [user_email])
        .then(([rows]) => {
          bcrypt.compare(user_pass, rows[0].password).then(compare_result => {
            if(compare_result === true) {
              const token = jwt.sign({ 
                id: rows[0].id,
                username: rows[0].username,
                email: rows[0].email,
                avatar: rows[0].avatar 
              }, jwtKey, {
                algorithm: "HS256",
                expiresIn: jwtExpirySeconds,
              })
              res.cookie("token", token)
              resolveJWT(token, res, true);
            }
            else{
              res.status(400).send('Invalid password!');
            }
          })
          .catch(err => {
            if (err) res.status(400).send(err);
          });
        }).catch(err => {
          if (err) res.status(400).send(err);
        });
      }).catch(err => {
        if (err) res.status(400).send(err);
      });
    })
    .catch(err => {
      if (err) res.status(400).send(err);
    })
  }
  else{
    res.status(400).send('Email is taken!');
  }
});

app.get('/authed', cors(corsOptions), (req, res) => {
  const token = req.headers.authorization.split(' ')[1];
  resolveJWT(token, res);
})

app.post('/', cors(corsOptions), [
  body('user_email').custom((value) => {
    return dbConnection.execute('SELECT `email` FROM `users` WHERE `email`=?', [value])
    .then(([rows]) => {
      if(rows.length == 1) {
        return true;
      }
      return Promise.reject('Invalid Email Address!');
    });
  }),
  body('user_pass', 'Password is empty!').trim().not().isEmpty(),
], (req, res) => {
  const validation_result = validationResult(req);
  const {user_pass, user_email} = req.body;
  if(validation_result.isEmpty()){
    dbConnection.execute("SELECT * FROM `users` WHERE `email`=?", [user_email])
    .then(([rows]) => {
      bcrypt.compare(user_pass, rows[0].password).then(compare_result => {
        if(compare_result === true){
          const token = jwt.sign({ 
            id: rows[0].id,
            username: rows[0].username,
            email: rows[0].email,
            avatar: rows[0].avatar 
          }, jwtKey, {
            algorithm: "HS256",
            expiresIn: jwtExpirySeconds,
          })
          res.cookie("token", token)
          resolveJWT(token, res, true);
        }
        else{
          res.status(400).send('Invalid password!');
        }
      }).catch(err => {
        if (err) res.status(400).send(err);
      });
    }).catch(err => {
      if (err) res.status(400).send(err);
    });
  }
  else{
    res.status(400).send('Invalid email!');
  }
});

app.post('/selectUser', cors(corsOptions), (req, res) => {
  const validation_result = validationResult(req);
  const {user_name, id} = req.body;
  if(validation_result.isEmpty()){
    dbConnection.execute("SELECT * FROM `users` WHERE `username` like CONCAT('%', ?, '%')", [user_name])
    .then(([rows]) => {
      let users = [];
      rows.map((row) => {
        if(id != row.id) {
          users.push({
            id: row.id,
            username: row.username,
            avatar: row.avatar
          });
        }
      });
      if(users.length == 0) {
        res.status(400).send({error: 'User not found'});
      } else {
        res.send(users);
      }
    }).catch(err => {
      if (err) res.status(400).send(err);
    });
  }
  else{
    res.status(400).send({error: 'User not found'});
  }
});

app.post('/createGroupChat', cors(corsOptions), async (req, res) => {
  const token = req.headers.authorization.split(' ')[1];
  const validation_result = validationResult(req);
  const {friends_id} = req.body;
  if(token) {
    let {id} = jwt.verify(token, jwtKey);
    if(validation_result.isEmpty()) {
      dbConnection.execute("INSERT INTO `chats`(`is_multiuser`) VALUES(?)", [true]);
      friends_id.map((friendId) => {
        dbConnection.query("INSERT INTO `chat_users`(`chat_id`,`user_id`) VALUES(?)", friendId);
      })
      const r = await getAllChats(id);
      res.send(r);
    } else {
      res.status(400).send('Unknown error');
    }      
  }
});

app.post('/deleteUser', cors(corsOptions), (req, res) => {
  const token = req.headers.authorization.split(' ')[1];
  if(token) {
    let {id} = jwt.verify(token, jwtKey);
    if(validation_result.isEmpty()) {
      dbConnection.execute("DELETE FROM `users` WHERE `users`.`id` = (?)", [id])
      .then(result => {
        res.send('Delete is successful');
      }).catch(err => {
        if (err) res.status(400).send('Remove failed!');
      });
    } else {
      res.status(400).send('Unknown error');
    }      
  }
});

async function getAllChats(id) {
  let count = [], lastMessage = [];
  const result = await dbConnection.execute("SELECT `users`.`username`, `users`.`avatar`, `users`.`id`, `chats`.`chat_id`, `chats`.`name` FROM `chat_users` JOIN `users` ON `users`.`id` = `chat_users`.`user_id` JOIN `chats` ON `chat_users`.`chat_id` = `chats`.`chat_id` WHERE `chat_users`.`chat_id` IN(SELECT `chats`.`chat_id` FROM `chats` WHERE `chats`.`chat_id` IN(SELECT `chat_users`.`chat_id` FROM `chat_users` WHERE `chat_users`.`user_id` = (?))) AND `chat_users`.`user_id` != (?)", [id, id]);

  await Promise.all(result[0].map(async chat => {
    const rt = await dbConnection.execute("SELECT `messages`.`chat_id`, COUNT(`messages`.`is_read`) AS count FROM `messages` WHERE `messages`.`chat_id` = (?) AND `messages`.`user_id` != (?) AND `messages`.`is_read` = 0 GROUP BY `messages`.`chat_id`", [chat.chat_id, id]);
    const rp = await dbConnection.execute("SELECT `messages`.`contect`, `messages`.`chat_id` FROM `messages` WHERE `messages`.`chat_id` = (?) ORDER BY `messages`.`date_create` DESC LIMIT 1", [chat.chat_id]);
    count.push(...rt[0])
    lastMessage.push(...rp[0]);
  }))
  return {result: result[0], count, lastMessage};
}

async function getAllMessages(chat_id) {
  const result = await dbConnection.execute("SELECT `messages`.`message_id`, `messages`.`contect`, `messages`.`date_create`, `messages`.`is_read`, `users`.`username`, `users`.`id` FROM `messages` JOIN `users` ON `messages`.`user_id` = `users`.`id` WHERE `messages`.`chat_id` = (?) ORDER BY `messages`.`date_create`", [chat_id])
  
  return result[0];
}

app.post('/dataEditing', cors(corsOptions), async (req, res) => {
  const validation_result = validationResult(req);
  let {user_name, user_pass, user_email} = req.body;
  const token = req.headers.authorization.split(' ')[1];
  if(token) {
    let {id} = jwt.verify(token, jwtKey), oldEmail;
    const rows = await dbConnection.execute('SELECT * FROM `users` WHERE `id`=?', [id])

    oldEmail = rows[0][0].email;
    if(user_email.trim().length === 0) {
      user_email = rows[0][0].email;
    } else {
      if(rows[0][0].email != user_email) {
        const rt = await dbConnection.execute('SELECT `email` FROM `users` WHERE `email`=?', [user_email])
        if(rt[0].length > 0){
          user_email = oldEmail;
          return res.status(400).send('This E-mail already in use!');
        } 
      }
    }

    let check = false;
    if(user_pass.trim().length === 0) {
      user_pass = rows[0][0].password;
      check = true;
    }

    if(user_name.trim().length === 0) {
      user_name = rows[0][0].username;
    }

    if(validation_result.isEmpty()) {
      userUpdate(user_name, user_email, user_pass, id, res, check);
    }
  }
});

async function userUpdate(user_name, user_email, user_pass, id, res, check) {
  if(check) {
    await dbConnection.execute("UPDATE `users` SET `users`.`username` = (?), `users`.`email` = (?), `users`.`password` = (?) WHERE `users`.`id` = (?)", [user_name, user_email, user_pass, id])
  } else {
    await bcrypt.hash(user_pass, 12).then(async (hash_pass) => { 
      await dbConnection.execute("UPDATE `users` SET `users`.`username` = (?), `users`.`email` = (?), `users`.`password` = (?) WHERE `users`.`id` = (?)", [user_name, user_email, hash_pass, id])
    });
  }

  dbConnection.execute("SELECT * FROM `users` WHERE `id`=?", [id])
  .then(([rows]) => {
    const token = jwt.sign({ 
      id: rows[0].id,
      username: rows[0].username,
      email: rows[0].email,
      avatar: rows[0].avatar 
    }, jwtKey, {
      algorithm: "HS256",
      expiresIn: jwtExpirySeconds,
    })
    resolveJWT(token, res, true);
  })
  .catch(err => {
    if (err) res.status(400).send(err);
  });
}

async function resolveJWT(token, res) {
  if(token) {
    let payload = jwt.verify(token, jwtKey);
    let chat = await getAllChats(payload.id);
    res.send({
      user: {
        token: token,
        ...payload
      },
      chat: chat 
    })
  }
}

app.post('/update', cors(corsOptions), async (req, res) => {
  const token = req.headers.authorization.split(' ')[1];
  const validation_result = validationResult(req);
  const {chat_id} = req.body;
  let exists = false;
  if(token) {
    let payload = jwt.verify(token, jwtKey);    
    const msg = await getAllMessages(chat_id);
    let chat = await getAllChats(payload.id);
    res.send({
      user: {
        token: token,
        ...payload
      },
      chat: chat,
      messages: msg  
    })
  }
});

let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads');
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + Math.random(0, 9) + path.extname(file.originalname));
  }
})
 
const fileFilter = (req, file, cb) => {
  if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png' || file.mimetype == 'image/gif') {
    cb(null, true);
  } else {
    cb(null, false);
  }
}

const upload = multer({ storage: storage, fileFilter: fileFilter });

server.listen(PORT, () => console.log(`Server running on port ${PORT}`));

const io = require('socket.io')(server, {
  cors: {
    enabled: true,
    headers: '*',
    origin: ["http://localhost:3000", "http://25.88.24.170:3000"],
  },
});

let users = new Map();

io.on('connection', socket => {
  socket.on('connected', handleConnected);
  socket.on('message', handleMessage);

  function handleConnected(token) {
    if(token) {
      let {id} = jwt.verify(token, jwtKey);
      users.set(socket.id, id);
      console.log("New client(" + users.get(socket.id) + ") connected");
    }
  }

  async function handleMessage(message, chatId) {
    let now = new Date();
    const isReadMessages = await dbConnection.execute("UPDATE `messages` SET `messages`.`is_read` = 1  WHERE `messages`.`is_read` = 0 AND `messages`.`chat_id` = (?) AND `messages`.`user_id` != (?)", [chatId, users.get(socket.id)]);
    dbConnection.execute("INSERT INTO `messages`(`chat_id`,`user_id`,`contect`,`date_create`,`is_read`) VALUES(?,?,?,?,?)", [chatId, users.get(socket.id), message, now, 0])
    .then(result => {
      dbConnection.execute("SELECT `chat_users`.`user_id` FROM `chat_users` WHERE `chat_users`.`chat_id` = (?)", [chatId])
      .then(result => {
        result[0].map((item) => {
          users.forEach((value, key) => {
            if(value === item.user_id) {
              io.to(key).emit('sendMessages', chatId);
            }
          });
        });
      });
    });
  }

  app.post('/getChatMessages', cors(corsOptions), async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    const validation_result = validationResult(req);
    const {chat_id} = req.body;
    let exists = false;
    if(token) {
      let {id} = jwt.verify(token, jwtKey);
      const isReadMessages = await dbConnection.execute("UPDATE `messages` SET `messages`.`is_read` = 1  WHERE `messages`.`is_read` = 0 AND `messages`.`chat_id` = (?) AND `messages`.`user_id` != (?)", [chat_id, id]);
      res.send('access');
      dbConnection.execute("SELECT `chat_users`.`user_id` FROM `chat_users` WHERE `chat_users`.`chat_id` = (?)", [chat_id])
      .then(result => {
        result[0].map((item) => {
          users.forEach((value, key) => {
            if(value === item.user_id) {
              io.to(key).emit('sendMessages', chat_id);
            }
          });
        });
      });
    }
  });

  app.post('/deleteMessage', cors(corsOptions), async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    const validation_result = validationResult(req);
    const {message_id} = req.body;

    if(token) {
      let {id} = jwt.verify(token, jwtKey);
      const ft = await dbConnection.execute("SELECT `messages`.`chat_id` FROM `messages` WHERE `messages`.`message_id` = (?)", [message_id]);
      if(validation_result.isEmpty()) {
        dbConnection.execute("DELETE FROM `messages` WHERE `messages`.`message_id` = (?)", [message_id])
        .then(result => {
          res.send('access');
          dbConnection.execute("SELECT `chat_users`.`user_id` FROM `chat_users` WHERE `chat_users`.`chat_id` = (?)", [ft[0][0].chat_id])
          .then(result => {
            result[0].map((item) => {
              users.forEach((value, key) => {
                if(value === item.user_id) {
                  io.to(key).emit('sendMessages', ft[0][0].chat_id);
                }
              });
            });
          });
        }).catch(err => {
          if (err) res.status(400).send('Remove failed!');
        });
      } else {
        res.status(400).send('Unknown error');
      }      
    }
  });

  app.post('/uploadPhoto', cors(corsOptions), upload.single('picture'), async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    const validation_result = validationResult(req);
    let exists = false;
    if(token) {
      let {id} = jwt.verify(token, jwtKey);
      try {
        let imgUrl = req.file.path;
        await dbConnection.execute("UPDATE `users` SET `users`.`avatar` = (?)WHERE `users`.`id` = (?)", [imgUrl, id]);
        dbConnection.execute("SELECT * FROM `users` WHERE `users`.`id`= (?)", [id])
        .then(async ([rows]) => {
          const token = jwt.sign({ 
            id: rows[0].id,
            username: rows[0].username,
            email: rows[0].email,
            avatar: rows[0].avatar 
          }, jwtKey, {
            algorithm: "HS256",
            expiresIn: jwtExpirySeconds,
          })
          await resolveJWT(token, res, true);
          const pp = await dbConnection.execute("SELECT `chat_users`.`chat_id` FROM `chat_users` WHERE `chat_users`.`user_id` = (?)", [id]);
          await Promise.all(pp[0].map(async chat => {
            const rt = await dbConnection.execute("SELECT `chat_users`.`user_id` FROM `chat_users` WHERE `chat_users`.`chat_id` = (?)", [chat.chat_id]);
            await Promise.all(rt[0].map(async user => {
              users.forEach((value, key) => {
                if(value === user.user_id) {
                  io.to(key).emit('sendMessages', chat.chat_id);
                }
              });
            }));
          }));
        });
        //res.send('access');
      } catch (error) {
        res.status(400).send('Format: png, jpg or gif!');
      }
    }
  })

  app.post('/createChat', cors(corsOptions), async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    const validation_result = validationResult(req);
    const {friend_id} = req.body;
    let exists = false;
    if(token) {
      let {id} = jwt.verify(token, jwtKey);
      if(validation_result.isEmpty()) {
        const result = await dbConnection.execute("SELECT * FROM `chats` WHERE `is_multiuser` = (?)", [false]);
        await Promise.all(result[0].map(async chat => {
          const rt = await dbConnection.execute("SELECT * FROM `chat_users` WHERE `chat_id` = (?)", [chat.chat_id])
          let flag = false;
          rt[0].map(user => {
            if(flag) {
              if(user.user_id === id || user.user_id === friend_id) {
                exists = true;
              } 
            } else {
              if(user.user_id === id || user.user_id === friend_id) {
                flag = true;
              } else {
                flag = false;
              }
            }
          });
        }))         
        if(!exists) {
          dbConnection.execute("INSERT INTO `chats`(`is_multiuser`) VALUES(?)", [false])
          .then(result => {
            let values = [
              [result[0].insertId, id], 
              [result[0].insertId, friend_id]
            ]
            let chatId = result[0].insertId;
            dbConnection.query("INSERT INTO `chat_users`(`chat_id`,`user_id`) VALUES(?),(?)", values)
            .then(async result => {          
              await dbConnection.execute("SELECT `chat_users`.`user_id` FROM `chat_users` WHERE `chat_users`.`chat_id` = (?)", [chatId])
              .then(result => {
                result[0].map((item) => {
                  users.forEach((value, key) => {
                    if(value === item.user_id) {
                      io.to(key).emit('sendMessages', chatId);
                    }
                  });
                });
              });
              res.send('access');
            });
          }).catch(err => {
            if (err) res.status(400).send(err);
          });
        } else {
          res.status(400).send('Chat exists');
        }      
      }
      else{
        res.status(400).send('User not found');
      }
    }
  });

  app.post('/deleteChat', cors(corsOptions), async (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    const validation_result = validationResult(req);
    const {chat_id} = req.body;
    if(token) {
      let {id} = jwt.verify(token, jwtKey);
      if(validation_result.isEmpty()) {
        const pt = await dbConnection.execute("SELECT `chat_users`.`user_id` FROM `chat_users` WHERE `chat_users`.`chat_id` = (?)", [chat_id])
        dbConnection.execute("DELETE FROM `chats` WHERE `chats`.`chat_id` = (?)", [chat_id])
        .then(async result => {
          pt[0].map((item) => {
            users.forEach((value, key) => {
              if(value === item.user_id) {
                io.to(key).emit('sendMessages', 0);
              }
            });
          });
          res.send('access');
        }).catch(err => {
          if (err) res.status(400).send('Remove failed!');
        });
      } else {
        res.status(400).send('Unknown error');
      }      
    }
  });

  socket.on('disconnect', () => {
    users.delete(socket.id);
    console.log('a user disconnected');
  });  
});